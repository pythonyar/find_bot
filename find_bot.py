#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
from datetime import datetime, timedelta
import os
import sys
import tarfile
from time import strftime


DEFAULT_TAR = './events.tar.gz'
DEFAULT_OUTPUT = strftime("%d-%b-%Y-%H-%M-%S.txt")

# we assume that bot switches between pages fast
FAST_SWITCH_TIME = 30 #seconds
# let's say bots don't visit same pages
# althoug sometimes they do
UNIQUE_VISITS_RATIO = 0.97

users = {}

def parse_log_file(log_file_obj):
    global users
    for line in log_file_obj:
        line = str(line.strip(), 'utf-8')
        if '|' in line:
            time_str, u_id = line.split('|', 1)
            if '|' in u_id:
                u_id, page_id = u_id.split('|')
            else:
                page_id = ''
            if not page_id: continue
            datetime_object = datetime.strptime(time_str, "%Y-%m-%dT%H:%M:%S")
            users.setdefault(u_id,[]).append((datetime_object, page_id))

def rule_1(entries):
    uniqs = set([i[1] for i in entries])
    if float(len(uniqs))/len(entries) < UNIQUE_VISITS_RATIO:
        return False
    return rule_2(entries)

def rule_2(entries):
    delta = entries[-1][0] - entries[0][0]
    if delta.total_seconds()/len(entries) < FAST_SWITCH_TIME:
        return True
    return False

def write_down_bot_ids(output_file):
    bot_ids = [k for k, v in users.items() if rule_1(v)]
    if bot_ids:
        print('%s potential bot(s) found.' % len(bot_ids))
        with open(output_file, 'w') as o:
            o.write('\n'.join(bot_ids))
        print('Potential bot ids are written into file %s' % output_file)
    else:
        print('No potential bots found')

def process_archive(archive_name):
    with tarfile.open(archive_name, 'r:*') as tf:
        logs = tf.getnames()
        logs.sort()
        for log_name in logs:
            print(log_name)
            with tf.extractfile(log_name) as file_obj:
                parse_log_file(file_obj)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', default=DEFAULT_TAR, dest='tar_name',
                        help='Path to *.tar.gz file')
    parser.add_argument('-o', default=DEFAULT_OUTPUT, dest='output_file',
                        help='Output file name for potential bot id-s')
    oprtions = parser.parse_args()
    tar_name = oprtions.tar_name
    output_file = oprtions.output_file

    if not os.path.isfile(tar_name) or not tarfile.is_tarfile(tar_name):
        print("Given file name either doesn't exist or is not a tar archive")
        sys.exit()

    print('Starting procesing the archive...')
    process_archive(tar_name)
    print('%s user ids found.\nStarting filtering bots of them...' % len(users))
    write_down_bot_ids(output_file)
