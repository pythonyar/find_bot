# The scripts for walking through archived log and finding bot's entries
This project is written to be used under **python 3.5**

No aditional packages ot libraries needed

To run it type in terminal:
``` bash
python /<path_to_the_script_folder>/find_bot.py [-f <path_to_archive>] [-o <path_to_output_file>]
```

or:

``` bash
python /<path_to_the_script_folder>/find_bot_on_low_ram.py [-f <path_to_archive>] [-o <path_to_output_file>]
```

If archive name is not provided then script will try to find it in its own folder.

If output file name is not provided then script will generate the name from current time and put the file in its own folder.

`find_bot.py`  is a first-part version of my script (RAM eating). It biulds a dictionary with user id-s as keys and a list of user's entries as value. List of user's entries contains pairs of time objects and page id-s visited bu the user. This building of entire statictics data in the memory is the reason why this script is considered as RAM consuming one. Reading an archive is already RAM saving as it uses iterations through objects (archive and text files) and doesn't load everything in RAM.

Rules for indicating a bot in this script:

- if user visited us few times (e.g. 100 times) then it is a real user
- if user visited us too maty time (e.g. 10000 times) then it is a bot
- if first 100 or last 100 entries made too fast then it is a bot
- if there are too many fast switches between pages then it is a bot

In the repository there are examples of text files with bot id-s found by the scripts.