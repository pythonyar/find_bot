#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
from datetime import datetime, timedelta
import os
import multiprocessing as mp
import sys
import tarfile
from time import strftime


DEFAULT_TAR = './events.tar.gz'
DEFAULT_OUTPUT = strftime("%d-%b-%Y-%H-%M-%S.txt")

# we assume that bot switches between pages fast
FAST_SWITCH_TIME = 30 #seconds

# user_ids = set()

def parse_line(line):
    line = str(line.strip(), 'utf-8')
    if not '|' in line: return "", "", ""
    time_str, u_id = line.split('|', 1)
    if '|' in u_id:
        u_id, page_id = u_id.split('|')
    else:
        page_id = ''
    return time_str, u_id, page_id

def parse_log_file(log_file_obj):
    # visits, first, last = 0, None, None
    # for line in log_file_obj:
    #     time_str, u_id, page_id = parse_line(line)
    #     if u_id != user_id: continue
    #     visits += 1
    #     datetime_object = datetime.strptime(time_str, "%Y-%m-%dT%H:%M:%S")
    #     if visits == 1:
    #         first = datetime_object
    #         continue
    #     last = datetime_object
    # return visits, first, last

    users = {}
    for line in log_file_obj:
        time_str, u_id, page_id = parse_line(line)
        if not page_id: continue
        user_record = users.get(u_id, [0, None, None])
        user_record[0] += 1
        datetime_object = datetime.strptime(time_str, "%Y-%m-%dT%H:%M:%S")
        if user_record[0] == 1:
            user_record[1] = datetime_object
            users[u_id] = user_record
            continue
        user_record[2] = datetime_object
        users[u_id] = user_record
    print(len(users), 'users here')
    return users

def rule(record):
    delta = record[2] - record[1]
    if delta.total_seconds()/record[0] < FAST_SWITCH_TIME:
        return True
    return False

def write_down_bot_ids(users, output_file):
    # pool = mp.Pool(processes=mp.cpu_count())
    # results = [pool.apply_async(process_archive, args=(x, archive_name)) for x in user_ids]
    # output = [p.get() for p in results]
    # bot_ids = [bot for bot in output if bot]
    # if bot_ids:
    #     print('%s potential bot(s) found.' % len(bot_ids))
    #     with open(output_file, 'w') as o:
    #         o.write('\n'.join(bot_ids))
    #     print('Potential bot ids are written into file %s' % output_file)
    # else:
    #     print('No potential bots found')
    bot_ids = [k for k, v in users.items() if rule(v)]
    if bot_ids:
        print('%s potential bot(s) found.' % len(bot_ids))
        with open(output_file, 'w') as o:
            o.write('\n'.join(bot_ids))
        print('Potential bot ids are written into file %s' % output_file)
    else:
        print('No potential bots found')

def process_archive(archive_name):
    # print('id: ' + user_id)
    # res = []
    # with tarfile.open(archive_name, 'r:*') as tf:
    #     logs = tf.getnames()
    #     logs.sort()
    #     for log_name in logs:
    #         with tf.extractfile(log_name) as file_obj:
    #             res.append(parse_log_file(file_obj, user_id))
    # delta = res[-1][2] - res[0][1]
    # visits = reduce((lambda x, y: x+y), (i[0] for i in res))
    # if delta.total_seconds()/visits < FAST_SWITCH_TIME:
    #     return user_id
    # return ''
    users = {}
    with tarfile.open(archive_name, 'r:*') as tf:
        logs = tf.getnames()
        logs.sort()

        for log_name in logs:
            print(log_name)
            with tf.extractfile(log_name) as file_obj:
                users_part = parse_log_file(file_obj)
                for u_id in users_part.keys():
                    user_record = users.get(u_id, [0, None, None])
                    if user_record[0] == 0:
                        user_record[1] = users_part[u_id][1]
                    user_record[2] = users_part[u_id][2]
                    user_record[0] += users_part[u_id][0]
                    users[u_id] = user_record
    print('%s user ids found.\nStarting filtering bots of them...' % len(users))
    write_down_bot_ids(users, output_file)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', default=DEFAULT_TAR, dest='tar_name',
                        help='Path to *.tar.gz file')
    parser.add_argument('-o', default=DEFAULT_OUTPUT, dest='output_file',
                        help='Output file name for potential bot id-s')
    oprtions = parser.parse_args()
    tar_name = oprtions.tar_name
    output_file = oprtions.output_file

    if not os.path.isfile(tar_name) or not tarfile.is_tarfile(tar_name):
        print("Given file name either doesn't exist or is not a tar archive")
        sys.exit()

    print('Starting procesing the archive...')
    process_archive(tar_name)
